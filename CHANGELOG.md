# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.1.5 - 2024-12-18(16:24:33 +0000)

### Other

- Relax double init check for tests

## Release v2.1.4 - 2024-12-13(15:09:01 +0000)

### Other

- Test if client already initialised

## Release v2.1.3 - 2024-11-30(08:23:33 +0000)

### Other

- Closing a query unsubscribes from the wrong event

## Release v2.1.2 - 2024-11-21(12:39:50 +0000)

### Other

- Subscribe on specific query events

## Release v2.1.1 - 2024-11-08(11:53:56 +0000)

### Other

- Support closing query from within callback

## Release v2.1.0 - 2024-10-15(10:00:16 +0000)

### Other

- Add gmap_devices_createIfActive()

## Release v2.0.0 - 2024-07-25(09:12:16 +0000)

### Other

- Remove old link API

## Release v1.7.1 - 2024-05-17(08:02:03 +0000)

### Other

- gmap_device_flags_string don't start with null byte

## Release v1.7.0 - 2024-05-02(11:43:48 +0000)

### Other

- new linking API

## Release v1.6.4 - 2024-03-08(14:01:43 +0000)

### Other

- Documentation generation fails for libgmap-client

## Release v1.6.3 - 2024-01-09(15:52:54 +0000)

### Changes

- Revert "[ServiceID] Make ServiceID work with SOP GMAP"

## Release v1.6.2 - 2024-01-05(13:05:30 +0000)

### Changes

- [ServiceID] Make ServiceID work with SOP GMAP

## Release v1.6.1 - 2023-11-19(12:23:07 +0000)

### Other

- Increase timeout

## Release v1.6.0 - 2023-10-25(15:51:45 +0000)

### Other

- [amx][gmap] move non-generic functions out of libgmap-client remove ip funtions

## Release v1.5.3 - 2023-10-04(06:54:32 +0000)

### Other

- gMap: setAlternativeRules, removeAlternativeRules support

## Release v1.5.2 - 2023-08-24(10:13:33 +0000)

### Other

- gMap islinkedto support missing in lib gmap client

## Release v1.5.1 - 2023-07-31(13:35:08 +0000)

### Other

- [lib-gmap] Const casting instead of dyn casting in boolean functions

## Release v1.5.0 - 2023-06-15(12:33:48 +0000)

### Other

- [Useragent][EmbeddedFiltering] Create the new AMX Useragent Embeddedfiltering module

## Release v1.4.0 - 2023-04-28(11:53:58 +0000)

### Other

- [gmap][UPNPDiscovery] Add UPNP support in gMap

## Release v1.3.0 - 2023-03-24(12:29:21 +0000)

### Other

- gMap server: action RPCs

## Release v1.2.0 - 2023-03-24(08:30:13 +0000)

### Other

- gMap functions RPC not implemented

## Release v1.1.0 - 2023-03-09(12:04:15 +0000)

### Other

- gmap config event handling: server and client side

## Release v1.0.0 - 2023-02-23(10:03:38 +0000)

### Breaking

- Adapt to new setActive API with source and priority

## Release v0.11.3 - 2023-02-16(11:36:05 +0000)

### Other

- [libgmap-client] generated datamodel documentation is empty

## Release v0.11.2 - 2023-01-25(09:29:44 +0000)

## Release v0.11.1 - 2023-01-19(14:45:44 +0000)

### Other

- [amx][gmap] Expose mdns information in gMap

## Release v0.11.0 - 2023-01-18(16:17:03 +0000)

### Other

- gMap server: getParameters, getFirstParameter RPC

## Release v0.10.1 - 2023-01-05(12:09:12 +0000)

### Other

- [amx][gmap] gmap_devices_findByIp function uses incorrect search paths

## Release v0.10.0 - 2023-01-05(11:55:07 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v0.9.2 - 2022-12-16(08:41:07 +0000)

### Other

- [amx][gmap] Expose mdns information in gMap

## Release v0.9.1 - 2022-12-06(13:06:29 +0000)

### Other

- gMap Event handling

## Release v0.9.0 - 2022-12-01(08:49:05 +0000)

### Other

- Add IP query support

## Release v0.8.1 - 2022-11-30(10:41:10 +0000)

### Other

- [amx][gmap] mib-ip fails to load due to undefined symbol

## Release v0.8.0 - 2022-11-21(17:39:41 +0000)

### Other

- Add gmap_ip_device_get_addresses()

## Release v0.7.0 - 2022-11-07(11:38:04 +0000)

### Other

- Add gmap_devices_findByIp

## Release v0.6.0 - 2022-10-06(13:40:15 +0000)

### Other

- [amx][gmap] Define Name, setName(), delName() behaviour

## Release v0.5.2 - 2022-09-26(12:43:39 +0000)

### Other

- Do not spam log on ip not found

## Release v0.5.1 - 2022-09-09(11:19:53 +0000)

### Other

- Create unit tests for amx gMap Query feature

## Release v0.5.0 - 2022-09-01(12:46:47 +0000)

### New

- [prpl][gMap]RPC topology missing

## Release v0.4.1 - 2022-08-08(12:02:34 +0000)

### Other

- [prpl][gMap] gMap queries must be evaluated in the gMap server(not the lib)

## Release v0.4.0 - 2022-08-03(16:43:44 +0000)

### Other

- LAN Device must have a uuid as unique (instance)key

## Release v0.3.2 - 2022-07-12(15:13:14 +0000)

### Other

- Fix `gmap_devices_unlink` doing nothing

## Release v0.3.1 - 2022-05-19(12:51:45 +0000)

### Other

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when...

## Release v0.3.0 - 2022-01-24(19:02:40 +0000)

### New

- Add query functionality

## Release v0.2.3 - 2021-08-16(12:08:06 +0000)

### Fixes

- [GMAP_CLIENT] functions returns array while expecting a bool

## Release v0.2.2 - 2021-08-11(09:38:04 +0000)

### Fixes

- [GMAP-CLIENT] gmap_device_hasTag does not work

## Release v0.2.1 - 2021-08-05(13:28:36 +0000)

### Fixes

- Circular dependency libgmap-client and gmap-server

## Release v0.2.0 - 2021-07-23(09:16:49 +0000)

### New

- [GMAP-CLIENT] add calls to add Ipaddresses

### Fixes

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components

### Changes

- add -lsahtrace flag in test_defines

### Other

- [CI] Add libsahtrace as build dependency

## Release v0.1.6 - 2021-05-14(07:37:16 +0000)

### Changes

- Add FAKEROOT variable to packages makefile

## Release 0.1.5 - 2021-04-08(12:12:39 +0000)

### Changes

- move copybara to baf 
- Add README 

## Release 0.1.4 - 2021-03-25(08:35:19 +0100)

### Fixes

- Fixes binding for Devices.Config

## Release 0.1.3 - 2021-03-10(12:48:39 +0100)

### Fixes

- device set now passes parameters correctly
- Fix null return value when calling device get
- device get now has correct signature in the wrapper
- extend gmap\_devices\_createDevice with values argument

## Release 0.1.2 - 2021-02-19(13:49:24 +0100)

### New

- Added wrapper functions for device calls
- Added wrapper functions for devies link calls
- Traverse mode string/enum conversion is now part of the lib 

## Release 0.1.1 - 2020-12-14(08:56:40 +0100)

## Changes

- Added support for invalid prefix names

## Release 0.1.0 - 2020-12-10(09:44:30 +0100)

### New

- Added devices block/unblock/isBlocked + coverage.
- Added device setActive + coverage.

### Changes

- Migrate to SoFa Pipelines and BAF
- Removed unneeded parameter.

## Release 0.0.5 - 2020-08-21(15:50:29 +0000)

### New

- Added device setName + coverage.
- Added device setType + coverage.
- Added event macro's.

## Release 0.0.4 - 2020-08-14(12:37:20 +0000)

### Changes

- Moved gmap_traverse_mode_t to server application.
- Added status code.

## Release 0.0.3 - 2020-07-30(15:25:38 +0000)

### New

- Added tests for devices flags.
- Added test for devices find + rename.
- Added status codes.

## Release 0.0.2 - 2020-07-24(09:25:43 +0000)

### New

- Added tests.
- Config.create, save, load, get, set.
- Added more status codes.
- Added uncrustify pre commit hook.

## Release 0.0.1 - 2020-07-14(10:27:55 +0000)

### Fixes

- Removes hardcoded version number

### Changes

- Added more status codes.
- Increase test coverage.

## Release 0.0.0 - 2020-06-24(14:20:00 +0000)

### New

- Initial release
