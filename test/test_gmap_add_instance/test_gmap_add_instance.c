/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_gmap_add_instance.h"
#include "../common/mock.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_variant.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>

#include <gmap/gmap.h>


/**********************************************************
* Variable declarations
**********************************************************/
static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;


int test_gmap_add_instance_setup(AMXB_UNUSED void** state) {
    test_init_dummy_dm(&dm, &parser);

    bus_ctx = amxb_be_who_has("Devices");
    gmap_client_init(bus_ctx);

    return amxd_status_ok;
}

int test_gmap_add_instance_teardown(AMXB_UNUSED void** state) {
    test_clean_dummy_dm(&dm, &parser);
    bus_ctx = NULL;

    return amxd_status_ok;
}

void test_gmap_add_instance_without_params(AMXB_UNUSED void** state) {
    int32_t index = 0;
    amxc_string_t inst_path;
    amxd_object_t* instance = NULL;

    amxc_string_init(&inst_path, 0);

    index = gmap_add_instance("dummy", "IPv4Address.", NULL);

    amxc_string_appendf(&inst_path, "Devices.Device.dummy.IPv4Address.%d", index);
    instance = amxd_dm_findf(&dm, amxc_string_get(&inst_path, 0));
    assert_non_null(instance);

    amxc_string_clean(&inst_path);
}

void test_gmap_add_instance_with_params(AMXB_UNUSED void** state) {
    int32_t index = 0;
    char* address_source = NULL;
    amxc_var_t params;
    amxc_string_t inst_path;
    amxd_object_t* instance = NULL;

    amxc_var_init(&params);
    amxc_string_init(&inst_path, 0);

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Address", "192.168.1.10");
    amxc_var_add_key(cstring_t, &params, "Status", "not reachable");
    amxc_var_add_key(cstring_t, &params, "Scope", "unknown");
    amxc_var_add_key(cstring_t, &params, "AddressSource", "test");
    amxc_var_add_key(bool, &params, "Reserved", true);

    index = gmap_add_instance("dummy", "IPv4Address.", &params);

    amxc_string_appendf(&inst_path, "Devices.Device.dummy.IPv4Address.%d", index);
    instance = amxd_dm_findf(&dm, amxc_string_get(&inst_path, 0));
    assert_non_null(instance);

    address_source = amxd_object_get_value(cstring_t, instance, "AddressSource", NULL);
    assert_string_equal(address_source, "test");

    amxc_var_clean(&params);
    amxc_string_clean(&inst_path);
    free(address_source);
}
