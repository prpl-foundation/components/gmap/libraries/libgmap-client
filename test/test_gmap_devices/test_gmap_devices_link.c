/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_gmap_devices.h"


static amxb_bus_ctx_t dummy;

void test_gmap_linkAdd__normalcase(UNUSED void** state) {
    amxc_var_t args;
    test_amxb_call_mockdata_t mock_data = {0};
    amxc_var_init(&args);
    bool ok = false;

    // GIVEN gmap-client

    // EXPECT bus call to happen
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "linkAdd");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_STATUS_OK,
        .arguments_received = &args,
    };
    will_return(__wrap_amxb_call, &mock_data);

    // WHEN calling gmap_devices_linkAdd
    ok = gmap_devices_linkAdd("updev", "lowdev", "mytype", "mydatasource", 123);

    // THEN the RPC function got called with correct arguments
    assert_true(ok);
    assert_string_equal(GETP_CHAR(&args, "upper_device"), "updev");
    assert_string_equal(GETP_CHAR(&args, "lower_device"), "lowdev");
    assert_string_equal(GETP_CHAR(&args, "type"), "mytype");
    assert_string_equal(GETP_CHAR(&args, "datasource"), "mydatasource");

    amxc_var_clean(&args);
}

void test_gmap_linkAdd__invalid_args(UNUSED void** state) {
    assert_false(gmap_devices_linkAdd(NULL, "lowdev", "mytype", "mydatasource", 123));
    assert_false(gmap_devices_linkAdd("updev", NULL, "mytype", "mydatasource", 123));
    assert_false(gmap_devices_linkAdd("updev", "lowdev", "mytype", NULL, 123));
}

void test_gmap_linkAdd__no_type(UNUSED void** state) {
    amxc_var_t args;
    test_amxb_call_mockdata_t mock_data = {0};
    amxc_var_init(&args);
    bool ok = false;

    // GIVEN gmap-client

    // EXPECT bus call to happen
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "linkAdd");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_STATUS_OK,
        .arguments_received = &args,
    };
    will_return(__wrap_amxb_call, &mock_data);

    // WHEN calling gmap_devices_linkAdd without passing type
    ok = gmap_devices_linkAdd("updev", "lowdev", NULL, "mydatasource", 0);

    // THEN the RPC function got called with correct arguments
    assert_true(ok);
    assert_string_equal(GETP_CHAR(&args, "upper_device"), "updev");
    assert_string_equal(GETP_CHAR(&args, "lower_device"), "lowdev");
    assert_null(GETP_CHAR(&args, "type"));
    assert_string_equal(GETP_CHAR(&args, "datasource"), "mydatasource");

    amxc_var_clean(&args);
}

void test_gmap_linkAdd__call_fail(UNUSED void** state) {
    amxc_var_t args;
    test_amxb_call_mockdata_t mock_data = {0};
    amxc_var_init(&args);
    bool ok = false;

    // GIVEN gmap-client

    // EXPECT bus call to fail
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "linkAdd");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_ERROR_BACKEND_FAILED,
    };
    will_return(__wrap_amxb_call, &mock_data);

    // WHEN calling gmap_devices_linkAdd
    ok = gmap_devices_linkAdd("updev", "lowdev", "mytype", "mydatasource", 123);

    // THEN the RPC function invocation reported to have failed
    assert_false(ok);

    amxc_var_clean(&args);
}

void test_gmap_linkReplace__normalcase(UNUSED void** state) {
    amxc_var_t args;
    test_amxb_call_mockdata_t mock_data = {0};
    amxc_var_init(&args);
    bool ok = false;

    // GIVEN gmap-client

    // EXPECT bus call to happen
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "linkReplace");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_STATUS_OK,
        .arguments_received = &args,
    };
    will_return(__wrap_amxb_call, &mock_data);

    // WHEN calling gmap_devices_linkReplace
    ok = gmap_devices_linkReplace("updev", "lowdev", "mytype", "mydatasource", 512);

    // THEN the RPC function got called with correct arguments
    assert_true(ok);
    assert_string_equal(GETP_CHAR(&args, "upper_device"), "updev");
    assert_string_equal(GETP_CHAR(&args, "lower_device"), "lowdev");
    assert_string_equal(GETP_CHAR(&args, "type"), "mytype");
    assert_string_equal(GETP_CHAR(&args, "datasource"), "mydatasource");

    amxc_var_clean(&args);
}

void test_gmap_linkReplace__invalid_args(UNUSED void** state) {
    assert_false(gmap_devices_linkReplace(NULL, "lowdev", "mytype", "mydatasource", 123));
    assert_false(gmap_devices_linkReplace("updev", NULL, "mytype", "mydatasource", 123));
    assert_false(gmap_devices_linkReplace("updev", "lowdev", "mytype", NULL, 123));
}

void test_gmap_linkReplace__no_type(UNUSED void** state) {
    amxc_var_t args;
    test_amxb_call_mockdata_t mock_data = {0};
    amxc_var_init(&args);
    bool ok = false;

    // GIVEN gmap-client

    // EXPECT bus call to happen
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "linkReplace");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_STATUS_OK,
        .arguments_received = &args,
    };
    will_return(__wrap_amxb_call, &mock_data);

    // WHEN calling gmap_devices_linkReplace without passing type
    ok = gmap_devices_linkReplace("hgw", "lanbridge", NULL, "self", 654);

    // THEN the RPC function got called with correct arguments
    assert_true(ok);
    assert_string_equal(GETP_CHAR(&args, "upper_device"), "hgw");
    assert_string_equal(GETP_CHAR(&args, "lower_device"), "lanbridge");
    assert_null(GETP_CHAR(&args, "type"));
    assert_string_equal(GETP_CHAR(&args, "datasource"), "self");

    amxc_var_clean(&args);
}

void test_gmap_linkReplace__call_fail(UNUSED void** state) {
    amxc_var_t args;
    test_amxb_call_mockdata_t mock_data = {0};
    amxc_var_init(&args);
    bool ok = false;

    // GIVEN gmap-client

    // EXPECT bus call to fail
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "linkReplace");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_ERROR_BACKEND_FAILED,
    };
    will_return(__wrap_amxb_call, &mock_data);

    // WHEN calling gmap_devices_linkReplace
    ok = gmap_devices_linkReplace("updev", "lowdev", "mytype", "mydatasource", 123);

    // THEN the RPC function invocation reported to have failed
    assert_false(ok);

    amxc_var_clean(&args);
}

void test_gmap_linkRemove__normalcase(UNUSED void** state) {
    amxc_var_t args;
    test_amxb_call_mockdata_t mock_data = {0};
    amxc_var_init(&args);
    bool ok = false;

    // GIVEN gmap-client

    // EXPECT bus call to happen
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "linkRemove");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_STATUS_OK,
        .arguments_received = &args,
    };
    will_return(__wrap_amxb_call, &mock_data);

    // WHEN calling gmap_devices_linkRemove
    ok = gmap_devices_linkRemove("ethport", "phone", "mod-eth-dev");

    // THEN the RPC function got called with correct arguments
    assert_true(ok);
    assert_string_equal(GETP_CHAR(&args, "upper_device"), "ethport");
    assert_string_equal(GETP_CHAR(&args, "lower_device"), "phone");
    assert_string_equal(GETP_CHAR(&args, "datasource"), "mod-eth-dev");

    amxc_var_clean(&args);
}

void test_gmap_linkRemove__invalid_args(UNUSED void** state) {
    assert_false(gmap_devices_linkRemove(NULL, "lowdev", "mydatasource"));
    assert_false(gmap_devices_linkRemove("updev", NULL, "mydatasource"));
    assert_false(gmap_devices_linkRemove("updev", "lowdev", NULL));
}

void test_gmap_linkRemove__call_fail(UNUSED void** state) {
    amxc_var_t args;
    test_amxb_call_mockdata_t mock_data = {0};
    amxc_var_init(&args);
    bool ok = false;

    // GIVEN gmap-client

    // EXPECT bus call to fail
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "linkRemove");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_ERROR_BACKEND_FAILED,
    };
    will_return(__wrap_amxb_call, &mock_data);

    // WHEN calling gmap_devices_linkRemove
    ok = gmap_devices_linkRemove("updev", "lowdev", "mydatasource");

    // THEN the RPC function invocation reported to have failed
    assert_false(ok);

    amxc_var_clean(&args);
}
