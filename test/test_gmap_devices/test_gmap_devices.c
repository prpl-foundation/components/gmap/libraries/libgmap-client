/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>
#include <debug/sahtrace.h>

#include <gmap/gmap.h>
#include <gmap/gmap_devices.h>

#include "test_gmap_devices.h"

#define UNUSED __attribute__((unused))

test_amxb_call_mockdata_t test_amxb_call_mockdata_return_c_ok = {.c_retval = AMXB_STATUS_OK};
test_amxb_call_mockdata_t test_amxb_call_mockdata_return_c_error = {.c_retval = AMXB_ERROR_INTERNAL};

static amxb_bus_ctx_t dummy;

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     int timeout) {
    test_amxb_call_mockdata_t* mockdata = (test_amxb_call_mockdata_t*) mock();
    check_expected_ptr(bus_ctx);
    check_expected_ptr(object);
    check_expected_ptr(method);
    check_expected(timeout);

    assert_int_equal(amxc_var_type_of(args), AMXC_VAR_ID_HTABLE);
    amxc_var_init(ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    if(mockdata->bus_retval_string != NULL) {
        amxc_var_add(cstring_t, ret, mockdata->bus_retval_string);
    } else {
        amxc_var_add(bool, ret, true);
    }

    if(mockdata->out_params != NULL) {
        const amxc_htable_t* out_params_htable = amxc_var_constcast(amxc_htable_t, mockdata->out_params);
        assert_non_null(out_params_htable);
        amxc_var_add(amxc_htable_t, ret, out_params_htable);
    }

    if(mockdata->expect_parameter1 != NULL) {
        amxc_var_t* actual_argument = GET_ARG(args, mockdata->expect_parameter1);
        assert_non_null(actual_argument);
        if(mockdata->expect_argument1_string != NULL) {
            assert_int_equal(AMXC_VAR_ID_CSTRING, amxc_var_type_of(actual_argument));
            assert_string_equal(amxc_var_constcast(cstring_t, actual_argument), mockdata->expect_argument1_string);
        } else {
            fail_msg("You'll have to extend this if you need other types");
        }
    } else {
        assert_null(mockdata->expect_argument1_string);
    }

    if(mockdata->arguments_received != NULL) {
        amxc_var_copy(mockdata->arguments_received, args);
    }

    return mockdata->c_retval;
}

int test_gmap_setup(UNUSED void** state) {
    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_ERROR);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    gmap_client_init(&dummy);

    return 0;
}

int test_gmap_teardown(UNUSED void** state) {
    sahTraceClose();

    return 0;
}

amxb_bus_ctx_t* test_gmap_get_bus_ctx(void) {
    return &dummy;
}
