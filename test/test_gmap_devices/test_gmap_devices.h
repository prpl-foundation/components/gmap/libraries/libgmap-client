/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __TEST_GMAP_DEVICES_H__
#define __TEST_GMAP_DEVICES_H__

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include <gmap/gmap.h>
#include <gmap/gmap_devices.h>
#include <gmap/gmap_devices_flags.h>

#define UNUSED __attribute__((unused))

/** opaque */
typedef struct {
    /** return value of the `amxb_call` C function */
    int c_retval;
    /** return value of the bus function (NULL if it's not a string) */
    const char* bus_retval_string;

    /** Name of parameter that is expected */
    const char* expect_parameter1;
    /** Argument value of `expect_parameter1` if it's a string */
    const char* expect_argument1_string;

    const amxc_var_t* out_params;

    /** If non-null, the mock of amxb_call will place the RPC arguments here */
    amxc_var_t* arguments_received;
} test_amxb_call_mockdata_t;

extern test_amxb_call_mockdata_t test_amxb_call_mockdata_return_c_ok;
extern test_amxb_call_mockdata_t test_amxb_call_mockdata_return_c_error;

amxb_bus_ctx_t* test_gmap_get_bus_ctx(void);

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     int timeout);

int test_gmap_setup(void** state);
int test_gmap_teardown(void** state);

void test_gmap_create_device(void** state);
void test_gmap_destroy_device(void** state);

void test_gmap_createDeviceOrGetKey__success(void** state);
void test_gmap_createDeviceOrGetKey__failure(void** state);

void test_gmap_createIfActive__success_exists_active(void** state);
void test_gmap_createIfActive__success_nonexists_active(void** state);
void test_gmap_createIfActive__success_exists_inactive(void** state);
void test_gmap_createIfActive__success_nonexists_inactive(void** state);
void test_gmap_createIfActive__failure_exists_active(void** state);
void test_gmap_createIfActive__failure_nonexists_active(void** state);
void test_gmap_createIfActive__failure_exists_inactive(void** state);
void test_gmap_createIfActive__failure_nonexists_inactive(void** state);
void test_gmap_createIfActive__null_for_output_params(void** state);
void test_gmap_createIfActive__invalid_arguments(void** state);

void test_gmap_findByMac__success(void** state);
void test_gmap_findByMac__not_found(void** state);
void test_gmap_findByMac__failure(void** state);

void test_gmap_find(void** state);

void test_gmap_block(void** state);
void test_gmap_unblock(void** state);
void test_gmap_is_blocked(void** state);

void test_gmap_link(void** state);
void test_gmap_unlink(void** state);
void test_gmap_set_link(void** state);

void test_gmap_linkAdd__normalcase(void** state);
void test_gmap_linkAdd__invalid_args(void** state);
void test_gmap_linkAdd__no_type(void** state);
void test_gmap_linkAdd__call_fail(void** state);
void test_gmap_linkReplace__normalcase(void** state);
void test_gmap_linkReplace__invalid_args(void** state);
void test_gmap_linkReplace__no_type(void** state);
void test_gmap_linkReplace__call_fail(void** state);
void test_gmap_linkRemove__normalcase(void** state);
void test_gmap_linkRemove__invalid_args(void** state);
void test_gmap_linkRemove__call_fail(void** state);

void test_gmap_devices_flags_from_cstring(void** state);
void test_gmap_device_string_from_flags(void** state);

#endif // __TEST_GMAP_DEVICES_H__
