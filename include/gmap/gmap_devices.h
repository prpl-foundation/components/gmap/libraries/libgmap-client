/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAP_DEVICES_H__)
#define __GMAP_DEVICES_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <amxc/amxc.h>


#if !defined(USE_DOXYGEN)
#define GMAP_INLINE static inline
#else
#define GMAP_INLINE
#endif

/**
 * Unconditionally create a device with given key
 *
 * In many cases you want to use @ref gmap_devices_createIfActive instead
 */
bool gmap_devices_createDevice(const char* key,
                               const char* discovery_source,
                               const char* tags,
                               bool persistent,
                               const char* default_name);
/**
 * Unconditionally create a device with given key and parameters
 *
 * In many cases you want to use @ref gmap_devices_createIfActive instead
 */
bool gmap_devices_createDevice_ext(const char* key,
                                   const char* discovery_source,
                                   const char* tags, bool persistent,
                                   const char* default_name, amxc_var_t* values);

/**
 * Create a device or returns key if device with mac already exists
 *
 * In many cases you want to use @ref gmap_devices_createIfActive instead
 */
bool gmap_devices_createDeviceOrGetKey(char** tgt_out_key, bool* tgt_out_already_exists,
                                       const char* mac, const char* discovery_source,
                                       const char* tags, bool persistent, const char* default_name, amxc_var_t* values);

/**
 * Creates a device if it does not exist and is active.
 *
 * - If the device already exists, the key of the existing device is placed in `tgt_out_key`.
 *   The tags are still set.
 * - If the device does not exist already, and is inactive, no new device is created.
 *   This is important because otherwise
 *   - devices that are manually deleted would be re-created again when it is observed that they
 *     are inactive. This observation can occur later than the deletion.
 *   - when we are close to max number of devices we want to remember, creating inactive devices
 *     is undesired.
 * - If the device does not exist already, and is active, a new device is created.
 *   The key of this new device is placed in `tgt_out_key`.
 *
 * @param tgt_dev_already_existed Will be true if the device already existed before this call
 *   (regardless of whether this call created the device).
 *
 * @return true if successful.
 *
 *   Note: the returnvalue does *not* indicate whether the device was created or not
 *
 *   Note: the returnvalue does *not* indicate whether the device already existed or not.
 *
 *   Note: the returnvalue being true does *not* imply `tgt_out_key` will be non-NULL.
 */
bool gmap_devices_createIfActive(char** tgt_out_key,
                                 bool* tgt_dev_already_existed,
                                 const char* dev_mac,
                                 const char* discovery_source,
                                 const char* dev_tags,
                                 bool persistent,
                                 const char* default_name,
                                 amxc_var_t* values,
                                 bool active);

bool gmap_devices_destroyDevice(const char* key);
bool gmap_devices_find(const char* expression,
                       uint32_t flags,
                       amxc_var_t* ret);
char* gmap_devices_findByMac(const char* mac);
char* gmap_devices_findByService(const char* instance);
bool gmap_devices_block(const char* key);
bool gmap_devices_unblock(const char* key);
bool gmap_devices_isBlocked(const char* key);

/**
 * @param priority Reserved parameter for potential future use.
 */
bool gmap_devices_linkAdd(const char* upper, const char* lower, const char* type, const char* datasource, uint32_t priority);
/**
 * @param priority Reserved parameter for potential future use.
 */
bool gmap_devices_linkReplace(const char* upper, const char* lower, const char* type, const char* datasource, uint32_t priority);
bool gmap_devices_linkRemove(const char* upper, const char* lower, const char* datasource);


#ifdef __cplusplus
}
#endif

#endif // __GMAP_DEVICES_H__
