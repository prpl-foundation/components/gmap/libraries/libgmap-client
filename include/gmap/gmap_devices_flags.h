/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAPS_FLAGS_H__)
#define __GMAPS_FLAGS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

/**
   @brief
   Do not add device details

   When this flag is used no device details (parameters) will be added to the returned result.
   This flag can be used in @ref gmap_device_get, @ref gmapc_device_get, @ref gmaps_device_get and
   @ref gmaps_device_getRecursive functions.

   Use function @ref gmap_device_flags_string to convert to a string representation.

   @see
   @ref GMAP_NO_DETAILS_STR
 */
#define GMAP_NO_DETAILS                          0x0002

/**
   @brief
   Do not add device actions

   When this flag is used no device actions will be added to the returned result.
   This flag can be used in @ref gmap_device_get, @ref gmapc_device_get, @ref gmaps_device_get and
   @ref gmaps_device_getRecursive functions.

   Use function @ref gmap_device_flags_string to convert to a string representation.

   @see
   @ref GMAP_NO_ACTIONS_STR
 */
#define GMAP_NO_ACTIONS                          0x0004

/**
   @brief
   Add child devices

   When this flag is used the toplogy (down direction) of the device will be added to the returned result.
   This flag can be used in @ref gmap_device_get, @ref gmapc_device_get, @ref gmaps_device_get and
   @ref gmaps_device_getRecursive functions.

   Use function @ref gmap_device_flags_string to convert to a string representation.

   @see
   @ref
   GMAP_INCLUDE_TOPOLOGY_STR
 */
#define GMAP_INCLUDE_TOPOLOGY                    0x0008

/**
   @brief
   Add the alternative devices

   When this flag is used the details of the alternative devices are added the result. The data structure of the master
   device will not contain any parameter (or union) of the alternative devices.

   When this flag is not used, a list of the alternative device keys is added to the result. The data structure of the
   master device will contain parameters (or union) of the alternative devices.

   Use function @ref gmap_device_flags_string to convert to a string representation.

   @see
   @ref GMAP_INCLUDE_ALTERNATIVES_STR
 */
#define GMAP_INCLUDE_ALTERNATIVES                0x0010

#define GMAP_INCLUDE_LINKS                       0x0020

#define GMAP_INCLUDE_FULL_LINKS                  0x0040

/**
   @brief
   String representation of flag @ref GMAP_NO_DETAILS

   When this flag is used no device details (parameters) will be added to the returned result.

   Use function @ref gmap_device_flags_from_string to convert to a bit mask.
 */
#define GMAP_NO_DETAILS_STR                      "no_details"

/**
   @brief
   String representation of flag @ref GMAP_NO_ACTIONS

   When this flag is used no device actions will be added to the returned result.

   Use function @ref gmap_device_flags_from_string to convert to a bit mask.
 */
#define GMAP_NO_ACTIONS_STR                      "no_actions"

/**
   @brief
   String representation of flag @ref GMAP_INCLUDE_TOPOLOGY

   When this flag is used the toplogy (down direction) of the device will be added to the returned result.

   Use function @ref gmap_device_flags_from_string to convert to a bit mask.
 */
#define GMAP_INCLUDE_TOPOLOGY_STR                "topology"

/**
   @brief
   String representation of flag @ref GMAP_INCLUDE_ALTERNATIVES

   When this flag is used the details of the alternative devices are added the result. The data structure of the master
   device will not contain any parameter (or union) of the alternative devices.

   When this flag is not used, a list of the alternative device keys is added to the result. The data structure of the
   master device will contain parameters (or union) of the alternative devices.

   Use function @ref gmap_device_flags_from_string to convert to a bit mask.
 */
#define GMAP_INCLUDE_ALTERNATIVES_STR            "alternatives"

#define GMAP_INCLUDE_LINKS_STR                   "links"

#define GMAP_INCLUDE_FULL_LINKS_STR              "full_links"

/**
   @brief
   Do not recurse devices that do not match the provided expression

   When this flag is used topology building does not include devices that do not match the provided expression or any
   of its children.
   This flag can be used in @ref gmap_device_topology, @ref gmapc_device_topology, @ref gmaps_device_topology

   Use function @ref gmap_device_topology_flags_string to convert to a string representation.

   @see
   @ref GMAP_TOPOLOGY_NO_RECURSE_STR
 */
#define GMAP_TOPOLOGY_NO_RECURSE                 0x0001

/**
   @brief
   Do not add device details

   When this flag is used no device details (parameters) will be added to the returned result.
   This flag can be used in @ref gmap_device_topology, @ref gmapc_device_topology, @ref gmaps_device_topology

   Use function @ref gmap_device_topology_flags_string to convert to a string representation.

   @see
   @ref GMAP_TOPOLOGY_NO_DETAILS_STR
 */
#define GMAP_TOPOLOGY_NO_DETAILS                 0x0002

/**
   @brief
   Do not add device actions

   When this flag is used no device actions will be added to the returned result.
   This flag can be used in @ref gmap_device_topology, @ref gmapc_device_topology, @ref gmaps_device_topology

   Use function @ref gmap_device_topology_flags_string to convert to a string representation.

   @see
   @ref GMAP_TOPOLOGY_NO_ACTIONS_STR
 */
#define GMAP_TOPOLOGY_NO_ACTIONS                 0x0004

/**
   @brief
   Add the alternative devices

   When this flag is used the details of the alternative devices are added the result. The data structure of the master
   device will not contain any parameter (or union) of the alternative devices.

   When this flag is not used, a list of the alternative device keys is added to the result. The data structure of the
   master device will contain parameters (or union) of the alternative devices.

   Use function @ref gmap_device_flags_string to convert to a string representation.

   @see
   @ref GMAP_TOPOLOGY_INCLUDE_ALTERNATIVES_STR
 */
#define GMAP_TOPOLOGY_INCLUDE_ALTERNATIVES       0x0010

#define GMAP_TOPOLOGY_INCLUDE_LINKS              0x0020

/**
   @brief
   Add the devices with a randomized mac address

   When this flag is used the details of the devices with a randomized mac address that are linked to a gmap placeholder device are added.
   Devices with a randomized mac address but that are not linked to a placeholder will never be added to the topology, even when using this flag.

   @deprecated
   This flag is deprecated and not supported by lib-gmap-client.
 */

#define GMAP_TOPOLOGY_INCLUDE_RANDOM_MAC         0x0080


/**
   @brief
   String representation of flag @ref GMAP_TOPOLOGY_NO_RECURSE

   When this flag is used topology building does not include devices that do not match the provided expression or any
   of its children.

   Use function @ref gmap_device_topology_flags_from_string to convert to a bit mask.
 */
#define GMAP_TOPOLOGY_NO_RECURSE_STR             "no_recurse"

/**
   @brief
   String representation of flag @ref GMAP_TOPOLOGY_NO_DETAILS

   When this flag is used no device details (parameters) will be added to the returned result.

   Use function @ref gmap_device_topology_flags_from_string to convert to a bit mask.
 */
#define GMAP_TOPOLOGY_NO_DETAILS_STR             "no_details"

/**
   @brief
   String representation of flag @ref GMAP_TOPOLOGY_NO_ACTIONS

   When this flag is used no device actions will be added to the returned result.

   Use function @ref gmap_device_topology_flags_from_string to convert to a bit mask.
 */
#define GMAP_TOPOLOGY_NO_ACTIONS_STR             "no_actions"

/**
   @brief
   String representation of flag @ref GMAP_TOPOLOGY_INCLUDE_ALTERNATIVES

   When this flag is used the details of the alternative devices are added the result. The data structure of the master
   device will not contain any parameter (or union) of the alternative devices.

   When this flag is not used, a list of the alternative device keys is added to the result. The data structure of the
   master device will contain parameters (or union) of the alternative devices.

   Use function @ref gmap_device_topology_flags_from_string to convert to a bit mask.
 */
#define GMAP_TOPOLOGY_INCLUDE_ALTERNATIVES_STR   "alternatives"

#define GMAP_TOPOLOGY_INCLUDE_LINKS_STR          "links"


/**
   @ingroup gmap_device
   @brief
   Converts a string containing flags into a bit field

   The string must contain device flags, separated with a '|' (pipe) sign.

   The following string are converted into a bit field:
   - "no_details" - @ref GMAP_NO_DETAILS
   - "no_actions" - @ref GMAP_NO_ACTIONS
   - "topology" - @ref GMAP_INCLUDE_TOPOLOGY
   - "alternatives" - @ref GMAP_INCLUDE_ALTERNATIVES
   - "links" - @ref GMAP_INCLUDE_LINKS
   - "full_links" - @ref GMAP_INCLUDE_FULL_LINKS

   Any other value is not converted into a bit field

   @param flags_string The string containing the flags

   @return
   Returns the corresponding bit field
 */
uint32_t gmap_devices_flags_from_cstring(const char* flags);

/**
   @ingroup gmap_device
   @brief
   Converts flags into a string

   The following flags are converted into a string:
   - @ref GMAP_NO_DETAILS - "no_details"
   - @ref GMAP_NO_ACTIONS - "no_actions"
   - @ref GMAP_INCLUDE_TOPOLOGY - "topology"
   - @ref GMAP_INCLUDE_ALTERNATIVES - "alternatives"
   - @ref GMAP_INCLUDE_LINKS - "links"
   - @ref GMAP_INCLUDE_FULL_LINKS - "full_links"

   All flags are separated with a '|' (pipe) sign.

   Any other value is not added to the string.

   @note
   - The returned pointer must be freed

   @param flags The bit field containing the flags

   @return
   Returns the corresponding string,
   or the empty string if no valid flags could be converted.
 */
amxc_string_t* gmap_device_flags_string(uint32_t flags);

#ifdef __cplusplus
}
#endif

#endif // __GMAPS_FLAGS_H__
