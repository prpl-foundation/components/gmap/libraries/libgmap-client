/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAP_DEVICE_H__)
#define __GMAP_DEVICE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>

#include "gmap_common_types.h"

#if !defined(USE_DOXYGEN)
#define GMAP_INLINE static inline
#else
#define GMAP_INLINE
#endif


/**
   @brief
   Notification id used when a device name is changed.

   When the selected name of a device is changed a notification is send to the client(s) using this as the notification type.
 */
#define GMAP_DEVICE_NAME_CHANGED                 111

/**
   @brief
   Notification id used when a new name is added (new name source).

   This notification is only send when a new instance of the Names sub-object is created.
 */
#define GMAP_DEVICE_SUB_NAME_ADDED                   112

/**
   @brief
   Notification id used when a name is removed (removed a name source).

   This notification is only send when an instance of the Names sub-object is deleted.
 */
#define GMAP_DEVICE_SUB_NAME_DELETED                 113

/**
   @brief
   Notification id used when a name is changed (changed then name provided by a name source).

   This notification is only send when the Name parameter of an instance of the Names sub-object is changed.
 */
#define GMAP_DEVICE_SUB_NAME_CHANGED                 114

/**
   @brief
   Notification id used when a function call is set
 */

#define GMAP_DEVICE_FUNCTION_CALL                120

/**
   @brief
   Notification name used when a device name is changed.

   When the selected name of a device is changed a notification is send to the client(s) using this as the notification name.
 */
#define GMAP_DEVICE_NAME_CHANGED_TEXT            "device_name_changed"

/**
   @brief
   Notification name used when a new name is added (new name source).

   This notification is only send when a new instance of the Names sub-object is created.
 */
#define GMAP_DEVICE_SUB_NAME_ADDED_TEXT              "device_sub_name_added"

/**
   @brief
   Notification name used when a name is removed (removed a name source).

   This notification is only send when an instance of the Names sub-object is deleted.
 */
#define GMAP_DEVICE_SUB_NAME_DELETED_TEXT            "device_sub_name_deleted"

/**
   @brief
   Notification name used when a name is changed (changed then name provided by a name source).

   This notification is only send when the Name parameter of an instance of the Names sub-object is changed.
 */
#define GMAP_DEVICE_SUB_NAME_CHANGED_TEXT            "device_sub_name_changed"

/**
   @brief
   Notification name used when a function call is set
 */

#define GMAP_DEVICE_FUNCTION_CALL_STR            "function_call"


typedef amxd_status_t (* gmap_function_handler_t) (amxc_var_t* device,
                                                   amxc_var_t* args,
                                                   amxc_var_t* ret,
                                                   uint64_t id);

bool gmap_device_setType(const char* key,
                         const char* type,
                         const char* source);

bool gmap_device_removeName(const char* key,
                            const char* source);
bool gmap_device_setName(const char* key,
                         const char* name,
                         const char* source);

bool gmap_device_setActive(const char* key,
                           bool active,
                           const char* source,
                           uint32_t priority);

/**
 * To be able to use any key for a device, we need to change non complient object names to complient object names
 * In order to do this we use a Prefix.
 * Object names should start with a letter
 */
#define GMAP_DEVICE_VALID_OBJECT_PREFIX "ID-"

/**
 *  Function creates valid object name from key.
 *  If Key is valid, a copy of key is returned
 */
char* gmap_create_valid_object_name(const char* key);

/**
 *  Function creates an object name to corresponding key
 *  If object name is key a copy is returned
 */
char* gmap_create_key_from_object_name(const char* objectname);

bool gmap_device_setTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode);
bool gmap_device_clearTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode);
bool gmap_device_hasTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode);

amxc_var_t* gmap_device_get(const char* key, uint32_t flags);
bool gmap_device_set(const char* key, amxc_var_t* values);

bool gmap_device_is_function_implemented(const char* key, const char* sub_object, const char* function);
bool gmap_device_set_function(const char* key, const char* sub_object, const char* function, const char* module, gmap_function_handler_t fn, amxc_var_t* data);
bool gmap_device_remove_function(const char* key, const char* sub_object, const char* function);
void gmap_device_function_done(uint64_t id, amxd_status_t state, amxc_var_t* retval);
void gmap_device_removeFunctionsModule(const char* module);
void gmap_device_removeFunctions(const char* key);

bool gmap_device_setAlternative(const char* master, const char* alternative);
bool gmap_device_removeAlternative(const char* master, const char* alternative);
bool gmap_device_isAlternative(const char* master, const char* alternative);

bool gmap_device_setAlternativeRules(const char* key, amxc_var_t* rules);
bool gmap_device_removeAlternativeRules(const char* key);

bool gmap_device_add_action(const char* key, const char* function_name, char* action_name);
bool gmap_device_remove_action(const char* key, const char* function_name, char* action_name);

amxc_var_t* gmap_device_topology(const char* key, const char* expression, gmap_traverse_mode_t mode, const char* flags);
amxc_var_t* gmap_device_get_first_parameter(const char* key, const char* parameter, const char* expression, gmap_traverse_mode_t mode);
amxc_var_t* gmap_device_get_parameters(const char* key, const char* parameter, const char* expression, gmap_traverse_mode_t mode);

bool gmap_device_isLinkedTo(const char* key, const char* device, gmap_traverse_mode_t mode);

int32_t gmap_add_instance(const char* key, const char* path, amxc_var_t* values);
int32_t gmap_delete_instance(const char* key, const char* path, uint32_t index);
int32_t gmap_get_subobject_instance(const char* key, const char* path, int32_t depth, amxc_var_t* ret);
int32_t gmap_set_subobject_instance(const char* key, const char* path, amxc_var_t* values);

#ifdef __cplusplus
}
#endif

#endif // __GMAP_DEVICE_H__
