/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <gmap/gmap_main.h>
#include <gmap/gmap_device.h>
#include <gmap/gmap_event.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include "gmap_priv.h"

#define ME "libgmap"

typedef struct _gmap_client_fn {
    amxc_llist_it_t it;
    char* key;
    char* function;
    char* module;
    gmap_function_handler_t fn;
    char* sub_object;
} gmap_client_fn_t;

static bool gmap_add_device_fn(const char* key, const char* sub_object, const char* function, const char* module, gmap_function_handler_t fn) {

    bool retval = false;
    amxc_llist_t* function_list = gmap_get_client_device_functions();

    gmap_client_fn_t* fn_data = calloc(1, sizeof(gmap_client_fn_t));
    when_null_trace(fn_data, exit, ERROR, "alloc failed");

    SAH_TRACEZ_INFO(ME, "Add function %s implementation for device %s", function, key);
    fn_data->key = strdup(key);
    fn_data->function = strdup(function);
    fn_data->module = strdup(module);
    fn_data->fn = fn;

    if(sub_object && (*sub_object)) {
        fn_data->sub_object = strdup(sub_object);
    }

    if(!fn_data->key || !fn_data->function || !fn_data->module
       || (sub_object && *sub_object && !fn_data->sub_object)) {
        free(fn_data->key);
        free(fn_data->function);
        free(fn_data->module);
        free(fn_data->sub_object);
        free(fn_data);
        SAH_TRACEZ_ERROR(ME, "strdup failed");
    }

    amxc_llist_append(function_list, &fn_data->it);

    retval = true;

exit:
    return retval;
}

static gmap_client_fn_t* gmap_get_device_fn(const char* key, const char* sub_object, const char* function) {
    gmap_client_fn_t* fn_data = NULL;
    amxc_llist_t* function_list = gmap_get_client_device_functions();

    amxc_llist_for_each(it, function_list) {
        fn_data = amxc_container_of(it, gmap_client_fn_t, it);
        if((strcmp(key, fn_data->key) == 0) &&
           (strcmp(function, fn_data->function) == 0) &&
           (((!sub_object || !(*sub_object)) && !fn_data->sub_object) ||
            (sub_object && fn_data->sub_object && (strncmp(sub_object, fn_data->sub_object, strlen(fn_data->sub_object)) == 0)))) {
            break;
        }
        fn_data = NULL;
    }

    return fn_data;
}

static void gmap_del_device_fn(const char* key, const char* sub_object, const char* function) {

    gmap_client_fn_t* fn_data = NULL;
    amxc_llist_t* function_list = gmap_get_client_device_functions();

    amxc_llist_for_each(it, function_list) {
        fn_data = amxc_container_of(it, gmap_client_fn_t, it);
        if((strcmp(key, fn_data->key) == 0) &&
           (strcmp(function, fn_data->function) == 0) &&
           (((!sub_object || !(*sub_object)) && !fn_data->sub_object) ||
            (sub_object && fn_data->sub_object && (strncmp(sub_object, fn_data->sub_object, strlen(sub_object)) == 0)))) {
            break;
        }
        fn_data = NULL;
    }

    if(fn_data) {
        amxc_llist_it_take(&fn_data->it);
        free(fn_data->key);
        free(fn_data->function);
        free(fn_data->module);
        free(fn_data->sub_object);
        free(fn_data);
    }
}

void gmap_device_function_done(uint64_t id, amxd_status_t state, amxc_var_t* retval) {

    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint64_t, &args, "ID", id);
    amxc_var_add_key(uint32_t, &args, "status", state);
    amxc_var_t* retval_var = amxc_var_add_new_key(&args, "retval");
    amxc_var_copy(retval_var, retval);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "csiFinished",
                       &args,
                       &ret,
                       gmap_get_timeout());
    when_failed_trace(status, exit, ERROR, "amxb call csiFinished failed");
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}


static void gmap_device_function(const char* key, uint32_t event_id, const amxc_var_t* data) {
    uint64_t id = amxc_var_dyncast(uint64_t, GET_ARG(data, "ID"));
    const char* fn_name = GET_CHAR(data, "Function");
    amxc_var_t* args = GET_ARG(data, "Args");
    amxc_var_t* device = GET_ARG(data, "Device");
    const char* sub_object = GET_CHAR(data, "ObjectPath");
    gmap_client_fn_t* device_fn = NULL;
    amxc_var_t retval;
    amxd_status_t fn_status = 0;

    when_str_empty_trace(key, exit, ERROR, "empty key");
    when_str_empty_trace(fn_name, exit, ERROR, "empty function name");
    if(event_id != GMAP_DEVICE_FUNCTION_CALL) {
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "CSI implementation called ....");
    SAH_TRACEZ_INFO(ME, "    CSI ID = %d", (uint32_t) id);
    SAH_TRACEZ_INFO(ME, "    Function = %s", fn_name);
    SAH_TRACEZ_INFO(ME, "    Device = %s", GET_CHAR(device, "Key"));

    device_fn = gmap_get_device_fn(key, sub_object, fn_name);
    when_null_trace(device_fn, exit, ERROR, "Function not found");

    amxc_var_init(&retval);

    // call the function
    fn_status = device_fn->fn(device, args, &retval, id);
    SAH_TRACEZ_INFO(ME, " CSI function done (state = %d)", fn_status);

    if(fn_status != amxd_status_deferred) {
        gmap_device_function_done(id, fn_status, &retval);
    }
    amxc_var_clean(&retval);

exit:
    return;
}


bool gmap_device_is_function_implemented(const char* key, const char* sub_object, const char* function) {
    int status = 0;
    bool retval = false;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, retval = false);
    when_str_empty_status(function, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "function", function);

    if(sub_object && *sub_object) {
        amxc_var_add_key(cstring_t, &args, "subObject", sub_object);
    }

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "isImplemented",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = GETI_BOOL(&ret, 0);
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

bool gmap_device_set_function(const char* key, const char* sub_object, const char* function, const char* module, gmap_function_handler_t fn, amxc_var_t* data) {
    int status = 0;
    bool retval = false;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_trace(key, exit, ERROR, "NULL/Empty");
    when_str_empty_trace(function, exit, ERROR, "NULL/Empty");
    when_str_empty_trace(module, exit, ERROR, "NULL/Empty");
    when_null_trace(fn, exit, ERROR, "NULL");

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "module", module);
    amxc_var_add_key(cstring_t, &args, "function", function);

    if(sub_object && *sub_object) {
        amxc_var_add_key(cstring_t, &args, "subObject", sub_object);
    }

    if(data) {
        amxc_var_t* data_var = amxc_var_add_new_key(&args, "data");
        amxc_var_copy(data_var, data);
    }

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "setFunction",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = GETI_BOOL(&ret, 0);
    }
    when_false_trace(retval, exit, ERROR, "setFunction call failed");

    retval = gmap_event_addHandler(GMAP_DEVICE_FUNCTION_CALL, gmap_device_function);
    when_false_trace(retval, exit, ERROR, "gmap event add handler error");

    retval = gmap_add_device_fn(key, sub_object, function, module, fn);
    when_false_trace(retval, exit, ERROR, "gmap add device fn error");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;

}

bool gmap_device_remove_function(const char* key, const char* sub_object, const char* function) {
    int status = 0;
    bool retval = false;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;
    amxc_llist_t* function_list = gmap_get_client_device_functions();

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_trace(key, exit, ERROR, "NULL/Empty");
    when_str_empty_trace(function, exit, ERROR, "NULL/Empty");

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "function", function);

    if(sub_object && *sub_object) {
        amxc_var_add_key(cstring_t, &args, "subObject", sub_object);
    }

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "removeFunction",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = GETI_BOOL(&ret, 0);
    }
    when_false_trace(retval, exit, ERROR, "setFunction call failed");

    gmap_del_device_fn(key, sub_object, function);

    if(amxc_llist_is_empty(function_list)) {
        gmap_event_removeHandler(GMAP_DEVICE_FUNCTION_CALL, gmap_device_function);
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

void gmap_device_removeFunctions(const char* key) {
    amxc_llist_t* function_list = gmap_get_client_device_functions();
    amxc_llist_it_t* it = amxc_llist_get_first(function_list);
    amxc_llist_it_t* prefetch = NULL;
    while(it) {
        prefetch = amxc_llist_it_get_next(it);
        gmap_client_fn_t* fn_data = amxc_container_of(it, gmap_client_fn_t, it);
        if(strcmp(key, fn_data->key) == 0) {
            gmap_device_remove_function(fn_data->key, fn_data->sub_object, fn_data->function);
        }
        fn_data = NULL;
        it = prefetch;
    }
}

void gmap_device_removeFunctionsModule(const char* module) {
    amxc_llist_t* function_list = gmap_get_client_device_functions();
    amxc_llist_it_t* it = amxc_llist_get_first(function_list);
    amxc_llist_it_t* prefetch = NULL;
    while(it) {
        prefetch = amxc_llist_it_get_next(it);
        gmap_client_fn_t* fn_data = amxc_container_of(it, gmap_client_fn_t, it);
        if(strcmp(module, fn_data->module) == 0) {
            gmap_device_remove_function(fn_data->key, fn_data->sub_object, fn_data->function);
        }
        fn_data = NULL;
        it = prefetch;
    }
}

