/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <gmap/gmap_event.h>
#include <gmap/gmap_main.h>
#include <gmap/gmap_traverse.h>
#include <gmap/gmap_devices_flags.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include "gmap_priv.h"
#include <amxp/amxp_expression.h>
#include <amxd/amxd_object_expression.h>

#define ME "libgmap"

typedef struct _gmap_event_handler_t {
    amxc_llist_it_t it;
    uint32_t event_id;
    gmap_event_handler_fn_t fn;
} gmap_event_handler_t;

static void gmap_event_execute_handlers(UNUSED const char* const sig_name,
                                        const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    gmap_event_handler_t* handler = NULL;
    const char* key = GET_CHAR(data, "Key");
    uint32_t event_id = GET_UINT32(data, "EventId");
    amxc_var_t* event_data = GET_ARG(data, "Data");
    SAH_TRACEZ_INFO(ME, "event execute handlers for event: %d", event_id);

    amxc_llist_for_each(it, gmap_get_client_event_handlers()) {
        handler = amxc_container_of(it, gmap_event_handler_t, it);
        if(handler && (handler->fn) && (handler->event_id == event_id)) {
            SAH_TRACEZ_INFO(ME, "event handler found");
            handler->fn(key, event_id, event_data);
        }
        handler = NULL;
    }
}

static bool gmap_event_subscribe(void) {

    int ret = -1;
    amxc_string_t object;
    amxc_string_t expr;

    SAH_TRACEZ_INFO(ME, "gmap event subscribe");

    amxc_string_init(&object, 0);
    amxc_string_setf(&object, "Devices.Device.");

    amxc_string_init(&expr, 0);
    amxc_string_setf(&expr, "notification == 'gmap_event'");

    SAH_TRACEZ_INFO(ME, "object: %s, expr: %s", amxc_string_get(&object, 0), amxc_string_get(&expr, 0));

    ret = amxb_subscribe(gmap_get_bus_ctx(),
                         amxc_string_get(&object, 0),
                         amxc_string_get(&expr, 0),
                         gmap_event_execute_handlers,
                         NULL);
    when_failed_trace(ret, exit, ERROR, "Call gmap_event subscribe failed");

    gmap_event_set_subscribed(true);
exit:
    amxc_string_clean(&expr);
    amxc_string_clean(&object);
    return (ret == 0) ? true : false;
}

static bool gmap_event_unsubscribe(void) {
    amxc_string_t object;
    int ret = -1;
    amxc_string_init(&object, 0);
    amxc_string_setf(&object, "Devices.Device.");
    SAH_TRACEZ_INFO(ME, "unsubscribe gmap_event");
    ret = amxb_unsubscribe(gmap_get_bus_ctx(),
                           amxc_string_get(&object, 0),
                           gmap_event_execute_handlers,
                           NULL);
    if(ret) {
        SAH_TRACEZ_NOTICE(ME, "Call gmap_event event unsubscribe failed");
    }
    amxc_string_clean(&object);
    gmap_event_set_subscribed(false);
    return true;
}

static bool gmap_event_addHandlerHelper(uint32_t event_id, gmap_event_handler_fn_t fn) {
    bool ok = false;

    /* search the list of event handlers and check if the same event handler is already
       set for the specified event
     */
    gmap_event_handler_t* handler = NULL;
    amxc_llist_for_each(it, gmap_get_client_event_handlers()) {
        handler = amxc_container_of(it, gmap_event_handler_t, it);
        if(handler && (handler->fn == fn) && (handler->event_id == event_id)) {
            SAH_TRACEZ_NOTICE(ME, "same event handler for same event already configured");
            break;
        }
        handler = NULL;
    }

    if(handler) {
        /* handler was already added, do nothing but return true. */
        ok = true;
        goto exit;
    }

    /* allocate memory to store the function pointer and event handler */
    handler = calloc(1, sizeof(gmap_event_handler_t));
    when_null_trace(handler, exit, ERROR, "Failed to allocate memory to store handler function");

    SAH_TRACEZ_INFO(ME, "Adding event handler for event id = %d (function = %p)", event_id, fn);
    handler->fn = fn;
    handler->event_id = event_id;
    amxc_llist_append(gmap_get_client_event_handlers(), &handler->it);
    ok = true;
exit:
    return ok;
}

static bool gmap_event_removeHandlerHelper(uint32_t event_id, gmap_event_handler_fn_t fn) {
    bool ok = false;

    /* search the list of event handlers and check if the event handler was set for the specified event */
    gmap_event_handler_t* handler = NULL;
    amxc_llist_for_each(it, gmap_get_client_event_handlers()) {
        handler = amxc_container_of(it, gmap_event_handler_t, it);
        if(handler && (handler->fn == fn) && (handler->event_id == event_id)) {
            break;
        }
        handler = NULL;
    }

    if(!handler) {
        /* handler was not set for the event, set return value to true and leave */
        ok = true;
        goto exit;
    }
    /* remove event handler from the list and free up the memory */
    SAH_TRACEZ_INFO(ME, "Removing event handler for event id = %d (function = %p)", event_id, fn);
    amxc_llist_it_take(&handler->it);
    free(handler);
    ok = true;
exit:
    return ok;
}

/**
   @ingroup gmap_event
   @brief
   Adds an event handler for the given event.

   Multiple event handlers can be set on the same event.
   The same event handler can be set on multiple events.
   The same event handler can only be set once on an event.

   @param event_id the event id on which the handler needs to be set
   @param fn event handler function

   @return
   true when the event handler was set, false when setting the event handler failed
 */

bool gmap_event_addHandler(uint32_t event_id, gmap_event_handler_fn_t fn) {
    bool ok = false;
    when_null(fn, exit);

    ok = gmap_event_addHandlerHelper(event_id, fn);
    when_false_trace(ok, exit, ERROR, "can not add handler for event_id: %d", event_id);
    if(!gmap_event_already_subscribed()) {
        ok = gmap_event_subscribe();
        if(!ok) {
            gmap_event_removeHandlerHelper(event_id, fn);
        }
    }
exit:
    return ok;
}

/**
   @ingroup gmap_event
   @brief
   Removes an event handler for the given event.

   Removes a previously added event handler (see @ref gmap_event_addHandler) for the specified event id.
   If the event handler was not added, nothing is done .

   @param event_id the event id for which the handler needs to be removed
   @param fn event handler function

   @return
   true when the event handler was removed (or not set before), false when removing the event handler failed
 */
bool gmap_event_removeHandler(uint32_t event_id, gmap_event_handler_fn_t fn) {
    when_null(fn, exit);
    gmap_event_removeHandlerHelper(event_id, fn);
    if(amxc_llist_is_empty(gmap_get_client_event_handlers())) {
        gmap_event_unsubscribe();
    }
exit:
    return true;
}

/**
   @ingroup gmap_event
   @brief
   Create and send an PCB custom event for a certain device.

   Depending on the context from where this function is called (client or server context) the correct function is called.
   See @ref gmaps_event_send for the server side implementation and @ref gmapc_event_send for the client side implementation

   @param key the device key for which an event must be sent
   @param event_id the event id
   @param event_name the name of the event
   @param data the event data
 */

void gmap_event_send(const char* key, uint32_t event_id, const char* event_name, const amxc_var_t* data) {

    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty_trace(key, exit, NOTICE, "empty key can not send event: %d", event_id);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", key);
    amxc_var_add_key(uint32_t, &args, "id", event_id);
    if(event_name) {
        amxc_var_add_key(cstring_t, &args, "name", event_name);
    }
    if(data) {
        amxc_var_t* event_data = NULL;
        event_data = amxc_var_add_new_key(&args, "data");
        amxc_var_copy(event_data, data);
    }

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "notify",
                       &args,
                       &ret,
                       gmap_get_timeout());
    when_failed_trace(status, exit, NOTICE, "Call Devices notice failed for event_id: %d", event_id);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return;
}
