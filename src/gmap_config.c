/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <gmap/gmap_config.h>
#include <gmap/gmap_event.h>
#include <gmap/gmap_main.h>
#include "gmap_priv.h"

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#define CONFIG_PATH "Devices.Config."
#define ME "libgmap"

typedef struct _gmap_config_event_handler_t {
    amxc_llist_it_t it;
    const char* module;
    gmap_config_event_handler_fn_t fn;
} gmap_config_event_handler_t;

bool gmap_config_create(const char* module) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty_status(module, exit, retval = false);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "module", module);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices.Config",
                       "create",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, &ret);
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return retval;
}

bool gmap_config_set(const char* module,
                     const char* option,
                     const amxc_var_t* value) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* subvar = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty_status(module, exit, retval = false);
    when_str_empty_status(option, exit, retval = false);
    when_null_status(value, exit, retval = false);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "module", module);
    amxc_var_add_key(cstring_t, &args, "option", option);
    subvar = amxc_var_add_new_key(&args, "value");
    amxc_var_copy(subvar, value);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices.Config",
                       "set",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = true;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return retval;
}

amxc_var_t* gmap_config_get(const char* module,
                            const char* option) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t* ret;

    amxc_var_new(&ret);
    amxc_var_init(&args);

    when_str_empty_status(module, exit, status = 1);
    when_str_empty_status(option, exit, status = 1);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "module", module);
    amxc_var_add_key(cstring_t, &args, "option", option);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices.Config",
                       "get",
                       &args,
                       ret,
                       gmap_get_timeout());

exit:
    if(status != 0) {
        amxc_var_delete(&ret);
    }
    amxc_var_clean(&args);

    return ret;
}

bool gmap_config_save(const char* module) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty_status(module, exit, retval = false);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "module", module);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices.Config",
                       "save",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, &ret);
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return retval;
}

bool gmap_config_load(const char* module) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty_status(module, exit, retval = false);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "module", module);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices.Config",
                       "load",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, &ret);
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return retval;
}

static void gmap_config_event_execute_handlers(UNUSED const char* const sig_name,
                                               const amxc_var_t* const data,
                                               UNUSED void* const priv) {
    gmap_config_event_handler_t* handler = NULL;
    char* path = strdup(GET_CHAR(data, "object"));
    char* rel_path = path + strlen(CONFIG_PATH);
    char* module = strtok(rel_path, ".");

    when_str_empty(strstr(path, CONFIG_PATH), exit);

    amxc_llist_for_each(it, gmap_get_client_config_event_handlers()) {
        handler = amxc_container_of(it, gmap_config_event_handler_t, it);
        if(handler && (handler->fn) && (strcmp(handler->module, module) == 0)) {
            SAH_TRACEZ_INFO(ME, "event handler found");
            amxc_var_t* param_values = amxc_var_get_key(data, "parameters", AMXC_VAR_FLAG_DEFAULT);
            handler->fn(module, param_values);
        }
        handler = NULL;
    }

exit:
    free(path);
    return;
}

static bool gmap_config_event_subscribe(void) {
    int ret = -1;
    amxc_string_t object;
    amxc_string_t expr;

    SAH_TRACEZ_INFO(ME, "gmap event subscribe");

    amxc_string_init(&object, 0);
    amxc_string_setf(&object, CONFIG_PATH);

    amxc_string_init(&expr, 0);
    amxc_string_setf(&expr, "notification in ['dm:instance-added', 'dm:instance-removed', 'dm:object-changed']");

    SAH_TRACEZ_INFO(ME, "object: %s, expr: %s", amxc_string_get(&object, 0), amxc_string_get(&expr, 0));

    ret = amxb_subscribe(gmap_get_bus_ctx(),
                         amxc_string_get(&object, 0),
                         amxc_string_get(&expr, 0),
                         gmap_config_event_execute_handlers,
                         NULL);
    when_failed_trace(ret, exit, ERROR, "Call gmap_event subscribe failed");

    gmap_config_event_set_subscribed(true);
exit:
    amxc_string_clean(&expr);
    amxc_string_clean(&object);
    return (ret == 0) ? true : false;
}

static bool gmap_config_event_unsubscribe(void) {
    amxc_string_t object;
    int ret = -1;
    amxc_string_init(&object, 0);
    amxc_string_setf(&object, CONFIG_PATH);
    SAH_TRACEZ_INFO(ME, "unsubscribe gmap_event");
    ret = amxb_unsubscribe(gmap_get_bus_ctx(),
                           amxc_string_get(&object, 0),
                           gmap_config_event_execute_handlers,
                           NULL);
    if(ret) {
        SAH_TRACEZ_NOTICE(ME, "Call gmap_event event unsubscribe failed");
    }
    amxc_string_clean(&object);
    gmap_config_event_set_subscribed(false);
    return true;
}

static bool gmap_config_event_addHandlerHelper(const char* module, gmap_config_event_handler_fn_t fn) {
    bool ok = false;

    /* search the list of event handlers and check if the same event handler is already
       set for the specified event
     */
    gmap_config_event_handler_t* handler = NULL;
    amxc_llist_for_each(it, gmap_get_client_config_event_handlers()) {
        handler = amxc_container_of(it, gmap_config_event_handler_t, it);
        if(handler && (handler->fn == fn) && (strcmp(handler->module, module) == 0)) {
            SAH_TRACEZ_NOTICE(ME, "same event handler for same event already configured");
            break;
        }
        handler = NULL;
    }

    if(handler) {
        /* handler was already added, do nothing but return true. */
        ok = true;
        goto exit;
    }

    /* allocate memory to store the function pointer and event handler */
    handler = calloc(1, sizeof(gmap_config_event_handler_t));
    when_null_trace(handler, exit, ERROR, "Failed to allocate memory to store handler function");

    SAH_TRACEZ_INFO(ME, "Adding event handler for module = %s (function = %p)", module, fn);
    handler->fn = fn;
    handler->module = module;
    amxc_llist_append(gmap_get_client_config_event_handlers(), &handler->it);
    ok = true;
exit:
    return ok;
}

static bool gmap_config_event_removeHandlerHelper(const char* module, gmap_config_event_handler_fn_t fn) {
    bool ok = false;

    /* search the list of event handlers and check if the event handler was set for the specified event */
    gmap_config_event_handler_t* handler = NULL;
    amxc_llist_for_each(it, gmap_get_client_config_event_handlers()) {
        handler = amxc_container_of(it, gmap_config_event_handler_t, it);
        if(handler && (handler->fn == fn) && (handler->module == module)) {
            break;
        }
        handler = NULL;
    }

    if(!handler) {
        /* handler was not set for the event, set return value to true and leave */
        ok = true;
        goto exit;
    }
    /* remove event handler from the list and free up the memory */
    SAH_TRACEZ_INFO(ME, "Removing event handler for module = %s (function = %p)", module, fn);
    amxc_llist_it_take(&handler->it);
    free(handler);
    ok = true;
exit:
    return ok;
}

/**
   @ingroup gmap_config
   @brief
   Adds a config changed handler for the given configuration.

   Multiple changed handlers can be set on the same configuration.
   The same changed handler can be set on multiple configurations.
   The same changed handler can only be set once on a configuration.

   @warning
   Modules that call to this function must have a matching call to @ref gmap_config_removeHandler.
   Failing to do so could lead to crashes whenever the module is unloaded from memory.

   @param module name of the configuration
   @param fn changed handler function

   @return
   true when the changed handler was set, false when setting the changed handler failed
 */
bool gmap_config_event_addHandler(const char* module, gmap_config_event_handler_fn_t fn) {
    bool ok = false;
    when_null(fn, exit);

    ok = gmap_config_event_addHandlerHelper(module, fn);
    when_false_trace(ok, exit, ERROR, "can not add handler for module: %s", module);
    if(!gmap_config_event_already_subscribed()) {
        ok = gmap_config_event_subscribe();
        if(!ok) {
            gmap_config_event_removeHandlerHelper(module, fn);
        }
    }
exit:
    return ok;
}

/**
   @ingroup gmap_config
   @brief
   Removes an event handler for the given event.

   Removes a previously added event handler (see @ref gmap_config_event_addHandler) for the specified event id.
   If the event handler was not added, nothing is done .

   @param module name of the configuration
   @param fn event handler function

   @return
   true when the event handler was removed (or not set before), false when removing the event handler failed
 */
bool gmap_config_event_removeHandler(const char* module, gmap_config_event_handler_fn_t fn) {
    when_null(fn, exit);
    gmap_config_event_removeHandlerHelper(module, fn);
    if(amxc_llist_is_empty(gmap_get_client_config_event_handlers())) {
        gmap_config_event_unsubscribe();
    }
exit:
    return true;
}
