/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <gmap/gmap_devices_flags.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include "gmap_priv.h"


uint32_t gmap_devices_flags_from_cstring(const char* flags) {
    uint32_t ret = 0;
    amxc_llist_t flags_list;
    amxc_string_t flags_string;

    amxc_llist_init(&flags_list);
    amxc_string_init(&flags_string, 0);

    when_str_empty(flags, exit);

    amxc_string_appendf(&flags_string, "%s", flags);
    amxc_string_split_to_llist(&flags_string, &flags_list, '|');

    amxc_llist_for_each(it, &flags_list) {
        amxc_string_t* flag_string = amxc_string_from_llist_it(it);
        amxc_string_trim(flag_string, NULL);
        if(strcmp(amxc_string_get(flag_string, 0), GMAP_NO_DETAILS_STR) == 0) {
            ret |= GMAP_NO_DETAILS;
            continue;
        }
        if(strcmp(amxc_string_get(flag_string, 0), GMAP_NO_ACTIONS_STR) == 0) {
            ret |= GMAP_NO_ACTIONS;
            continue;
        }
        if(strcmp(amxc_string_get(flag_string, 0), GMAP_INCLUDE_TOPOLOGY_STR) == 0) {
            ret |= GMAP_INCLUDE_TOPOLOGY;
            continue;
        }
        if(strcmp(amxc_string_get(flag_string, 0), GMAP_INCLUDE_ALTERNATIVES_STR) == 0) {
            ret |= GMAP_INCLUDE_ALTERNATIVES;
            continue;
        }
        if(strcmp(amxc_string_get(flag_string, 0), GMAP_INCLUDE_LINKS_STR) == 0) {
            ret |= GMAP_INCLUDE_LINKS;
            continue;
        }
        if(strcmp(amxc_string_get(flag_string, 0), GMAP_INCLUDE_FULL_LINKS_STR) == 0) {
            ret |= GMAP_INCLUDE_FULL_LINKS;
            continue;
        }
        if(strcmp(amxc_string_get(flag_string, 0), GMAP_TOPOLOGY_NO_RECURSE_STR) == 0) {
            ret |= GMAP_TOPOLOGY_NO_RECURSE;
            continue;
        }
    }

exit:
    amxc_llist_clean(&flags_list, amxc_string_list_it_free);
    amxc_string_clean(&flags_string);
    return ret;
}


amxc_string_t* gmap_device_flags_string(uint32_t flags) {
    amxc_string_t* gmap_flags;
    amxc_string_new(&gmap_flags, 64);

    const char* sep = "|";
    size_t sep_length = 0;

    if(flags & GMAP_NO_DETAILS) {
        amxc_string_append(gmap_flags, sep, sep_length);
        amxc_string_append(gmap_flags, GMAP_NO_DETAILS_STR, strlen(GMAP_NO_DETAILS_STR));
        sep_length = 1;
    }
    if(flags & GMAP_NO_ACTIONS) {
        amxc_string_append(gmap_flags, sep, sep_length);
        amxc_string_append(gmap_flags, GMAP_NO_ACTIONS_STR, strlen(GMAP_NO_ACTIONS_STR));
        sep_length = 1;
    }
    if(flags & GMAP_INCLUDE_TOPOLOGY) {
        amxc_string_append(gmap_flags, sep, sep_length);
        amxc_string_append(gmap_flags, GMAP_INCLUDE_TOPOLOGY_STR, strlen(GMAP_INCLUDE_TOPOLOGY_STR));
        sep_length = 1;
    }
    if(flags & GMAP_INCLUDE_ALTERNATIVES) {
        amxc_string_append(gmap_flags, sep, sep_length);
        amxc_string_append(gmap_flags, GMAP_INCLUDE_ALTERNATIVES_STR, strlen(GMAP_INCLUDE_ALTERNATIVES_STR));
        sep_length = 1;
    }
    if(flags & GMAP_INCLUDE_LINKS) {
        amxc_string_append(gmap_flags, sep, sep_length);
        amxc_string_append(gmap_flags, GMAP_INCLUDE_LINKS_STR, strlen(GMAP_INCLUDE_LINKS_STR));
        sep_length = 1;
    }
    if(flags & GMAP_INCLUDE_FULL_LINKS) {
        amxc_string_append(gmap_flags, sep, sep_length);
        amxc_string_append(gmap_flags, GMAP_INCLUDE_FULL_LINKS_STR, strlen(GMAP_INCLUDE_FULL_LINKS_STR));
        // when after this flag other flags are added, please add the following line
        // sep_length = 1;
    }

    return gmap_flags;
}
