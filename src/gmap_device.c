/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdio.h>

#include <gmap/gmap_device.h>
#include <gmap/gmap_main.h>
#include <gmap/gmap_traverse.h>
#include <gmap/gmap_devices_flags.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include "gmap_priv.h"

#define ME "libgmap"


bool gmap_device_setType(const char* key,
                         const char* type,
                         const char* source) {
    int status = 0;
    bool retval = false;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, retval = false);
    when_str_empty_status(type, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "source", source);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "setType",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, &ret);
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

bool gmap_device_removeName(const char* key,
                            const char* source) {
    int status = 0;
    bool retval = false;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "source", source);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "removeName",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, &ret);
    }
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

bool gmap_device_setName(const char* key,
                         const char* name,
                         const char* source) {
    int status = 0;
    bool retval = false;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, retval = false);
    when_str_empty_status(name, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    amxc_var_add_key(cstring_t, &args, "source", source);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "setName",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, &ret);
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

bool gmap_device_setActive(const char* key,
                           bool active,
                           const char* source,
                           uint32_t priority) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, retval = false);
    when_str_empty_status(source, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "active", active);
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(uint32_t, &args, "priority", priority);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "setActive",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

bool gmap_device_setTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;
    const char* modestring = gmap_traverse_string(mode);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "tag", tags);
    amxc_var_add_key(cstring_t, &args, "expression", expression);
    amxc_var_add_key(cstring_t, &args, "traverse", modestring);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "setTag",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}


bool gmap_device_clearTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;
    const char* modestring = gmap_traverse_string(mode);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "tag", tags);
    amxc_var_add_key(cstring_t, &args, "expression", expression);
    amxc_var_add_key(cstring_t, &args, "traverse", modestring);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "clearTag",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

bool gmap_device_hasTag(const char* key, const char* tags, const char* expression, gmap_traverse_mode_t mode) {
    int status = 0;
    bool retval = false;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;
    const char* modestring = gmap_traverse_string(mode);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "tag", tags);
    amxc_var_add_key(cstring_t, &args, "expression", expression);
    amxc_var_add_key(cstring_t, &args, "traverse", modestring);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "hasTag",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, amxc_var_get_first(&ret));
    }


exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

amxc_var_t* gmap_device_get(const char* key, uint32_t flags) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t* ret;
    amxc_string_t obj_path;
    amxc_string_t* flag_string = NULL;

    flag_string = gmap_device_flags_string(flags);

    amxc_var_init(&args);
    amxc_var_new(&ret);
    amxc_string_init(&obj_path, 0);


    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    if(!amxc_string_is_empty(flag_string)) {
        amxc_var_add_key(cstring_t, &args, "flags", amxc_string_get(flag_string, 0));
    }

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "get",
                       &args,
                       ret,
                       gmap_get_timeout());


    if(status != amxd_status_ok) {
        amxc_var_delete(&ret);
        ret = NULL;
    }

    amxc_var_clean(&args);
    amxc_string_clean(&obj_path);
    amxc_string_delete(&flag_string);

    return ret;

}

bool gmap_device_set(const char* key, amxc_var_t* values) {
    int status = 0;
    bool retval = true;
    amxc_var_t ret;
    amxc_string_t obj_path;
    amxc_var_t args;
    const amxc_htable_t* hvalues = NULL;

    hvalues = amxc_var_constcast(amxc_htable_t, values);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_amxc_htable_t(&args, "parameters", hvalues);


    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, retval = false);
    when_null_status(values, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "set",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    amxc_string_clean(&obj_path);

    return retval;

}

bool gmap_device_setAlternative(const char* master, const char* alternative) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(master, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", master);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alternative", alternative);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "setAlternative",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

bool gmap_device_removeAlternative(const char* master, const char* alternative) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(master, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", master);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alternative", alternative);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "removeAlternative",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

bool gmap_device_isAlternative(const char* master, const char* alternative) {
    int status = 0;
    bool retval = false;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(master, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", master);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alternative", alternative);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "isAlternative",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, amxc_var_get_first(&ret));
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

bool gmap_device_setAlternativeRules(const char* key, amxc_var_t* rules) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, retval = false);
    when_null_status(rules, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_key(&args, "rules", rules, AMXC_VAR_FLAG_COPY);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "setAlternativeRules",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, amxc_var_get_first(&ret));
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

bool gmap_device_removeAlternativeRules(const char* key) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, retval = false);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "removeAlternativeRules",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, amxc_var_get_first(&ret));
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}

bool gmap_device_add_action(const char* key, const char* function_name, char* action_name) {
    int status = amxd_status_invalid_function_argument;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    when_str_empty(key, exit);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "function", function_name);
    amxc_var_add_key(cstring_t, &args, "name", action_name);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "addAction",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        SAH_TRACEZ_ERROR(ME, "Call to addAction failed with error code %d", status);
    }

exit:
    if(status != amxd_status_invalid_function_argument) {
        amxc_var_clean(&args);
    }
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return amxd_status_ok == status;
}

bool gmap_device_remove_action(const char* key, const char* function_name, char* action_name) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t obj_path;

    when_str_empty(key, exit);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "function", function_name);
    amxc_var_add_key(cstring_t, &args, "name", action_name);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "removeAction",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        SAH_TRACEZ_ERROR(ME, "Call to removeAction failed with error code %d", status);
    }

exit:
    if(status != amxd_status_invalid_function_argument) {
        amxc_var_clean(&args);
    }
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return amxd_status_ok == status;
}

amxc_var_t* gmap_device_topology(const char* key, const char* expression, gmap_traverse_mode_t mode, const char* flags) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* content = NULL;
    amxc_string_t obj_path;
    const char* modestring = gmap_traverse_string(mode);

    when_str_empty(key, exit);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flags", flags);
    amxc_var_add_key(cstring_t, &args, "expression", expression);
    amxc_var_add_key(cstring_t, &args, "traverse", modestring);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "topology",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status == amxd_status_ok) {
        content = amxc_var_get_index(&ret, 0, AMXC_VAR_FLAG_DEFAULT);
        amxc_var_take_it(content);
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    amxc_string_clean(&obj_path);

    return content;
}

amxc_var_t* gmap_device_get_first_parameter(const char* key, const char* parameter, const char* expression, gmap_traverse_mode_t mode) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* content = NULL;
    amxc_string_t obj_path;
    const char* modestring = gmap_traverse_string(mode);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty(key, exit);
    when_str_empty(parameter, exit);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "parameter", parameter);
    amxc_var_add_key(cstring_t, &args, "expression", expression);
    amxc_var_add_key(cstring_t, &args, "traverse", modestring);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "getFirstParameter",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status == amxd_status_ok) {
        content = amxc_var_get_index(&ret, 0, AMXC_VAR_FLAG_COPY);
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    amxc_string_clean(&obj_path);

    return content;
}

amxc_var_t* gmap_device_get_parameters(const char* key, const char* parameter, const char* expression, gmap_traverse_mode_t mode) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* content = NULL;
    amxc_string_t obj_path;
    const char* modestring = gmap_traverse_string(mode);


    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty(key, exit);
    when_str_empty(parameter, exit);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "parameter", parameter);
    amxc_var_add_key(cstring_t, &args, "expression", expression);
    amxc_var_add_key(cstring_t, &args, "traverse", modestring);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "getParameters",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status == amxd_status_ok) {
        content = amxc_var_get_index(&ret, 0, AMXC_VAR_FLAG_COPY);
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    amxc_string_clean(&obj_path);

    return content;
}

int32_t gmap_add_instance(const char* key, const char* path, amxc_var_t* values) {
    int32_t index = 0;
    int status = 0;
    amxc_var_t ret;
    amxc_string_t device_path;

    amxc_var_init(&ret);
    amxc_string_init(&device_path, 0);

    amxc_string_appendf(&device_path,
                        "Devices.Device.%s.%s.",
                        key, path);

    status = amxb_add(gmap_get_bus_ctx(), amxc_string_get(&device_path, 0), 0, NULL, values, &ret, gmap_get_timeout());

    if(status == 0) {
        index = GETP_INT32(&ret, "0.index");
    }

    amxc_var_clean(&ret);
    amxc_string_clean(&device_path);

    return index;
}

int32_t gmap_delete_instance(const char* key, const char* path, uint32_t index) {
    int status = 0;
    amxc_var_t ret;
    amxc_string_t device_path;

    amxc_var_init(&ret);
    amxc_string_init(&device_path, 0);

    if(index != 0) {
        amxc_string_appendf(&device_path,
                            "Devices.Device.%s.%s.%d.",
                            key, path, index);
    } else {
        amxc_string_appendf(&device_path,
                            "Devices.Device.%s.%s.",
                            key, path);
    }

    status = amxb_del(gmap_get_bus_ctx(), amxc_string_get(&device_path, 0), index, NULL, &ret, gmap_get_timeout());

    amxc_var_clean(&ret);
    amxc_string_clean(&device_path);

    return status;
}

int32_t gmap_get_subobject_instance(const char* key, const char* path, int32_t depth, amxc_var_t* ret) {
    int status = 0;
    amxc_string_t device_path;

    amxc_string_init(&device_path, 0);

    amxc_string_appendf(&device_path,
                        "Devices.Device.%s.%s.",
                        key, path);

    status = amxb_get(gmap_get_bus_ctx(), amxc_string_get(&device_path, 0), depth, ret, gmap_get_timeout());

    amxc_string_clean(&device_path);

    return status;
}

int32_t gmap_set_subobject_instance(const char* key, const char* path, amxc_var_t* values) {
    int32_t index = 0;
    int status = 0;
    amxc_var_t ret;
    amxc_string_t device_path;

    amxc_var_init(&ret);
    amxc_string_init(&device_path, 0);

    amxc_string_appendf(&device_path,
                        "Devices.Device.%s.%s.",
                        key, path);

    status = amxb_set(gmap_get_bus_ctx(), amxc_string_get(&device_path, 0), values, &ret, gmap_get_timeout());

    if(status == 0) {
        index = GETP_INT32(&ret, "0.index");
    }

    amxc_var_clean(&ret);
    amxc_string_clean(&device_path);

    return index;
}

bool gmap_device_isLinkedTo(const char* key, const char* device, gmap_traverse_mode_t mode) {
    int status = 0;
    amxc_string_t obj_path;
    amxc_var_t args;
    amxc_var_t ret;
    bool retval = false;
    const char* modestring = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_str_empty(key, exit);
    when_str_empty(device, exit);

    modestring = gmap_traverse_string(mode);

    amxc_string_appendf(&obj_path, "Devices.Device.%s", key);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "device", device);
    amxc_var_add_key(cstring_t, &args, "traverse", modestring);

    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&obj_path, 0),
                       "isLinkedTo",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, amxc_var_get_first(&ret));
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);

    return retval;
}
