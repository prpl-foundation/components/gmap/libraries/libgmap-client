/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define ME "gmapcld"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <gmap/gmap_device.h>
#include <gmap/gmap_devices.h>
#include <gmap/gmap_main.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxp/amxp_expression.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include "gmap_priv.h"

/**
 * Helper function to call RPC function that returns a boolean.
 */
static bool s_call_boolret(const char* function_name, amxc_var_t* args) {
    int status = 0;
    bool retval = false;
    amxc_var_t ret;
    amxc_var_init(&ret);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       function_name,
                       args,
                       &ret,
                       gmap_get_timeout());
    when_failed_trace(status, exit, ERROR, "Error amxb_call on %s", function_name);

    retval = amxc_var_dyncast(bool, &ret);

exit:
    amxc_var_clean(&ret);

    return retval;
}

static bool gmap_devices_findByMac_ext(char** tgt_device_key, const char* mac) {
    const char* const_key = NULL;
    char* key = NULL;
    int status = amxd_status_unknown_error;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty_trace(mac, exit, ERROR, "NULL mac parameter");
    when_null_trace(tgt_device_key, exit, ERROR, "NULL tgt_dev_key parameter");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "mac", mac);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "findByMac",
                       &args,
                       &ret,
                       gmap_get_timeout());
    when_failed_trace(status, exit, ERROR, "Error calling 'findByMac' for '%s': %i", mac, status);

    const_key = amxc_var_constcast(cstring_t, amxc_var_get_index(&ret, 0, AMXC_VAR_FLAG_DEFAULT));
    if((const_key == NULL) || (const_key[0] == '\0')) {
        // unclear if all busses will support nullable/optional return type,
        // so also unify empty string to NULL.
        key = NULL;
    } else {
        key = strdup(const_key);
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    if(tgt_device_key != NULL) {
        *tgt_device_key = key;
    }
    return status == amxd_status_ok;
}

bool gmap_devices_createDevice(const char* key,
                               const char* discovery_source,
                               const char* tags,
                               bool persistent,
                               const char* default_name) {
    return gmap_devices_createDevice_ext(key, discovery_source, tags, persistent, default_name, NULL);
}

bool gmap_devices_createDevice_ext(const char* key, const char* discovery_source, const char* tags, bool persistent, const char* default_name, amxc_var_t* values) {


    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty_status(key, exit, retval = false);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", key);
    amxc_var_add_key(cstring_t, &args, "discovery_source", discovery_source);
    amxc_var_add_key(cstring_t, &args, "tags", tags);
    amxc_var_add_key(bool, &args, "persistent", persistent);
    amxc_var_add_key(cstring_t, &args, "default_name", default_name);
    if(values) {
        amxc_var_set_key(&args, "values", values, AMXC_VAR_FLAG_COPY);
    }

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "createDevice",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, &ret);
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return retval;
}

bool gmap_devices_createDeviceOrGetKey(char** tgt_out_key, bool* tgt_out_already_exists,
                                       const char* mac, const char* discovery_source,
                                       const char* tags, bool persistent, const char* default_name, amxc_var_t* values) {

    int status = 0;
    bool retval = false;
    amxc_var_t args;
    amxc_var_t ret;
    bool call_retval = false;
    const amxc_var_t* call_out_args;
    const char* call_out_arg_key = NULL;
    bool call_out_arg_already_exist = false;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty_trace(mac, exit, ERROR, "'mac' missing");
    when_str_empty_trace(tags, exit, ERROR, "'tags' missing");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "mac", mac);
    amxc_var_add_key(cstring_t, &args, "discovery_source", discovery_source);
    amxc_var_add_key(cstring_t, &args, "tags", tags);
    amxc_var_add_key(bool, &args, "persistent", persistent);
    amxc_var_add_key(cstring_t, &args, "default_name", default_name);
    if(values != NULL) {
        amxc_var_set_key(&args, "values", values, AMXC_VAR_FLAG_COPY);
    }

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "createDeviceOrGetKey",
                       &args,
                       &ret,
                       gmap_get_timeout());

    when_failed_trace(status, exit, ERROR, "Error %i when calling createDeviceOrGetKey", status);
    call_retval = amxc_var_dyncast(bool, amxc_var_get_index(&ret, 0, AMXC_VAR_FLAG_DEFAULT));
    when_false_trace(call_retval, exit, ERROR, "createDeviceOrGetKey returned error");
    call_out_args = amxc_var_get_index(&ret, 1, AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(call_out_args, exit, ERROR, "createDeviceOrGetKey output arguments missing");
    call_out_arg_already_exist = GET_BOOL(call_out_args, "already_exists");
    call_out_arg_key = GET_CHAR(call_out_args, "key");
    when_str_empty_trace(call_out_arg_key, exit, ERROR, "createDeviceOrGetKey 'key' output argument missing");

    if(tgt_out_key != NULL) {
        *tgt_out_key = strdup(call_out_arg_key);
    }
    if(tgt_out_already_exists != NULL) {
        *tgt_out_already_exists = call_out_arg_already_exist;
    }
    retval = true;

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return retval;
}

bool gmap_devices_createIfActive(char** tgt_out_key, bool* tgt_dev_already_existed,
                                 const char* dev_mac,
                                 const char* discovery_source,
                                 const char* dev_tags,
                                 bool persistent,
                                 const char* default_name,
                                 amxc_var_t* values,
                                 bool active) {
    char* dev_key = NULL;
    bool dev_already_existed = false;
    bool ok = false;

    when_null_trace(dev_mac, exit, ERROR, "NULL argument mac");
    when_null_trace(discovery_source, exit, ERROR, "NULL argument discovery source");
    when_null_trace(dev_tags, exit, ERROR, "NULL argument discovery source");

    if(active) {
        ok = gmap_devices_createDeviceOrGetKey(&dev_key,
                                               &dev_already_existed,
                                               dev_mac,
                                               discovery_source,
                                               dev_tags,
                                               persistent,
                                               default_name,
                                               values);
        when_false_trace(ok, exit, ERROR, "Error calling gmap createDeviceOrGetKey");

        // If already exists, its tags might be out of date, so update
        if(dev_already_existed) {
            ok = gmap_device_setTag(dev_key, dev_tags, NULL, gmap_traverse_this);
            when_false_trace(ok, exit, ERROR, "Error setting tag %s %s", dev_mac, dev_key);
        }
    } else {
        ok = gmap_devices_findByMac_ext(&dev_key, dev_mac);
        when_false_trace(ok, exit, ERROR, "Error attempting to lookup key for mac %s", dev_mac);
        when_null(dev_key, exit);

        dev_already_existed = true;

        // The device might be missing some tags, so set them:
        ok = gmap_device_setTag(dev_key, dev_tags, NULL, gmap_traverse_this);
        when_false_trace(ok, exit, ERROR, "Error setting tag %s %s", dev_mac, dev_key);
    }

exit:
    if(tgt_dev_already_existed != NULL) {
        *tgt_dev_already_existed = dev_already_existed;
    }
    if(tgt_out_key != NULL) {
        *tgt_out_key = dev_key;
    } else {
        free(dev_key);
        dev_key = NULL;
    }
    return ok;
}

char* gmap_devices_findByMac(const char* mac) {
    char* key = NULL;
    gmap_devices_findByMac_ext(&key, mac);
    return key;
}


bool gmap_devices_destroyDevice(const char* key) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", key);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "destroyDevice",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, &ret);
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return retval;
}

bool gmap_devices_find(const char* expression,
                       uint32_t flags,
                       amxc_var_t* ret) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(ret);

    when_null_status(expression, exit, retval = false);
    when_null_status(ret, exit, retval = false);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "expression", expression);
    amxc_var_add_key(uint32_t, &args, "flags", flags);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "find",
                       &args,
                       ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    }

exit:
    amxc_var_clean(&args);

    return retval;
}

bool gmap_devices_block(const char* key) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty_status(key, exit, retval = false);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", key);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "block",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return retval;
}

bool gmap_devices_unblock(const char* key) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty_status(key, exit, retval = false);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", key);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "unblock",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return retval;
}

bool gmap_devices_isBlocked(const char* key) {
    int status = 0;
    bool retval = true;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty_status(key, exit, retval = false);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", key);

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "isBlocked",
                       &args,
                       &ret,
                       gmap_get_timeout());

    if(status != 0) {
        retval = false;
    } else {
        retval = amxc_var_dyncast(bool, &ret);
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return retval;
}

char* gmap_create_valid_object_name(const char* key) {
    char* retv = NULL;
    if(isalpha(key[0])) {
        retv = (char*) malloc(strlen(key) + 1);
        strcpy(retv, key);
    } else {
        int prefix_len = strlen(GMAP_DEVICE_VALID_OBJECT_PREFIX);
        retv = (char*) malloc(prefix_len + strlen(key) + 1);
        strcpy(retv, GMAP_DEVICE_VALID_OBJECT_PREFIX);
        strcpy(retv + prefix_len, key);
    }
    return retv;
}

char* gmap_create_key_from_object_name(const char* objectname) {
    char* retv = NULL;
    int prefix_len = strlen(GMAP_DEVICE_VALID_OBJECT_PREFIX);
    if(strncmp(GMAP_DEVICE_VALID_OBJECT_PREFIX, objectname, prefix_len) == 0) {
        retv = (char*) malloc(strlen(objectname) - prefix_len + 1);
        strcpy(retv, objectname + prefix_len);
    } else {
        retv = (char*) malloc(strlen(objectname) + 1);
        strcpy(retv, objectname);
    }

    return retv;

}

bool gmap_devices_linkAdd(const char* upper, const char* lower, const char* type, const char* datasource, UNUSED uint32_t priority) {
    bool retval = false;
    amxc_var_t args;
    amxc_var_init(&args);
    when_str_empty_trace(upper, exit, ERROR, "NULL/Empty argument");
    when_str_empty_trace(lower, exit, ERROR, "NULL/Empty argument");
    when_str_empty_trace(datasource, exit, ERROR, "NULL/Empty argument");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "upper_device", upper);
    amxc_var_add_key(cstring_t, &args, "lower_device", lower);
    if(type != NULL) {
        amxc_var_add_key(cstring_t, &args, "type", type);
    }
    amxc_var_add_key(cstring_t, &args, "datasource", datasource);

    retval = s_call_boolret("linkAdd", &args);

exit:
    amxc_var_clean(&args);

    return retval;
}

bool gmap_devices_linkReplace(const char* upper, const char* lower, const char* type, const char* datasource, UNUSED uint32_t priority) {
    bool retval = false;
    amxc_var_t args;
    amxc_var_init(&args);
    when_str_empty_trace(upper, exit, ERROR, "NULL/Empty argument");
    when_str_empty_trace(lower, exit, ERROR, "NULL/Empty argument");
    when_str_empty_trace(datasource, exit, ERROR, "NULL/Empty argument");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "upper_device", upper);
    amxc_var_add_key(cstring_t, &args, "lower_device", lower);
    if(type != NULL) {
        amxc_var_add_key(cstring_t, &args, "type", type);
    }
    amxc_var_add_key(cstring_t, &args, "datasource", datasource);

    retval = s_call_boolret("linkReplace", &args);

exit:
    amxc_var_clean(&args);

    return retval;
}

bool gmap_devices_linkRemove(const char* upper, const char* lower, const char* datasource) {
    bool retval = false;
    amxc_var_t args;
    amxc_var_init(&args);
    when_str_empty_trace(upper, exit, ERROR, "NULL/Empty argument");
    when_str_empty_trace(lower, exit, ERROR, "NULL/Empty argument");
    when_str_empty_trace(datasource, exit, ERROR, "NULL/Empty argument");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "upper_device", upper);
    amxc_var_add_key(cstring_t, &args, "lower_device", lower);
    amxc_var_add_key(cstring_t, &args, "datasource", datasource);

    retval = s_call_boolret("linkRemove", &args);

exit:
    amxc_var_clean(&args);

    return retval;
}
